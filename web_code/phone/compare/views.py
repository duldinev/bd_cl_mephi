from django.shortcuts import render
from .forms import CompForm, GetSolution
import mysql.connector
from mysql.connector import connect, Error
from django.http import HttpResponse

smart = {1: 'samsung',
         2: 'apple',
         3: 'xiaomi',
         4: 'nokia'}


def get_compare(requests):
    if requests.method == 'POST':
        dev1 = smart[int(requests.POST['device1'][0])]
        dev2 = smart[int(requests.POST['device2'][0])]

        print(dev2)
        print(dev1)

        inch = float(requests.POST['inches'])
        width = int(requests.POST['width'])
        height = int(requests.POST['height'])
        ram = int(requests.POST['ram'])
        hd = int(requests.POST['hd'])

        conn = connect(
            host="84.201.148.170",
            user='node2',
            password='Node1234!',
            database='user_free')

        # добавили запрос на получение
        cursor = conn.cursor(buffered=True)
        ex = f'insert into valQuery (dev1, dev2, screen, px1, px2, ram, hd) values ("{dev1}", "{dev2}",' \
             f' {inch}, {width}, {height}, {ram}, {hd})'
        cursor.execute(ex)
        conn.commit()

        # добавили поле для ответа
        ex = 'insert into result_val (dev1val, dev2val) values (-1, -1)'
        cursor.execute(ex)
        conn.commit()

        # добавили поле для номера
        ex = 'SELECT count(*) FROM result_val'
        cursor.execute(ex)
        num_el = cursor.fetchone()[0]

        cursor.close()
        conn.close()

        return HttpResponse(f'<h1>Введите данный номер для получения решения: {num_el} <h1>')

    form = CompForm()

    data = {
        'form': form
    }

    return render(requests, 'compare/main_page.html', data)


# поле для возвращения ответа
def get_solution(requests):
    message = ''

    if requests.method == 'POST':
        conn = connect(
            host="84.201.148.170",
            user='node2',
            password='Node1234!',
            database='user_free')

        cursor = conn.cursor(buffered=True)
        number = int(requests.POST['number_sol'])
        ex = f'select * from result_val where id={number}'
        cursor.execute(ex)

        cur = cursor.fetchone()
        if cur is None:
            return HttpResponse('<h1>Ожидайте<h1>')
        if cur[1] == -1:
            return HttpResponse('Node_1 is working')
        if cur[2] == -1:
            return HttpResponse('Node_2 is working')

        ex = f'select * from valQuery where id={cur[0]}'
        cursor.execute(ex)
        dev_name_ex = cursor.fetchone()

        # проверка на равенство по цене
        if dev_name_ex[1] == dev_name_ex[2]:
            return HttpResponse(f'<h1>{dev_name_ex[1]} равен по цене {dev_name_ex[2]}</h1>')

        # проверка на больше/меньше
        if cur[1] > cur[2]:
            return HttpResponse(f'<h1>{dev_name_ex[1]} дороже {dev_name_ex[2]}</h1>')
        elif cur[1] == cur[2]:
            return HttpResponse(f'<h1>{dev_name_ex[1]} равен по цене {dev_name_ex[2]}</h1>')
        else:
            return HttpResponse(f'<h1>{dev_name_ex[2]} дороже {dev_name_ex[1]}</h1>')

    form = GetSolution()

    data = {
        'form': form,
        'mess': message
    }

    return render(requests, 'compare/check results.html', data)
