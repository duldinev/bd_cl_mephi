from django.urls import path
from .views import get_compare, get_solution

urlpatterns = [
    path('', get_compare, name='main'),
    path('quest/', get_solution, name='solution'),
]
