from django import forms

smart = [
    (1, 'samsung'),
    (2, 'apple'),
    (3, 'xiaomi'),
    (4, 'nokia')
]


class CompForm(forms.Form):
    # device1 = forms.CharField(max_length=150, choices=smart)
    # device2 = forms.CharField(max_length=150, choices=smart)

    device1 = forms.CharField(
        max_length=130,
        widget=forms.Select(choices=smart),
    )

    device2 = forms.CharField(
        max_length=130,
        widget=forms.Select(choices=smart),
    )

    inches = forms.FloatField()
    width = forms.IntegerField()
    height = forms.IntegerField()
    ram = forms.IntegerField()
    hd = forms.IntegerField()

class GetSolution(forms.Form):
    number_sol = forms.IntegerField()
