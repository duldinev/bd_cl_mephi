from django.db import models

# Create your models here.


class CompareSmart(models.Model):

    smart = [
        (1, 'samsung'),
        (2, 'apple'),
        (3, 'xiaomi')
    ]

    dev1 = models.PositiveSmallIntegerField(choices=smart)
    dev2 = models.PositiveSmallIntegerField(choices=smart)

