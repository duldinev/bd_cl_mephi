import requests
from bs4 import BeautifulSoup
import pymongo
import time
import mysql.connector
from mysql.connector import connect, Error
# библиотеки из main
from main import start_parse_module

# запускаем скрипт на мониторинг системы
def start_infinite_loop():
    # conn = connect(
    #     host="84.201.148.170",
    #     user='node2',
    #     password='Node1234!',
    #     database='user_free')

    # cursor = conn.cursor(buffered=True)

    # выбираем последний индекс из таблицы "last_index"
    # и ищем строку с наименованием в таблице  valQuery
    # если строка найдена мы спарсим сайт и данные обработаем
    # и запищем результат в новую бд
    # иначе мы просто ждём
    index = 1
    while True:
        conn = connect(
            host="84.201.148.170",
            user='node2',
            password='Node1234!',
            database='user_free')
        cursor = conn.cursor(buffered=True)

        last_index = 'select * from last_index'
        cursor.execute(last_index)
        cursor_ind_val = cursor.fetchone()
        print(cursor_ind_val)

        if cursor_ind_val is not None:
            ex = f"select * from valQuery where id = {cursor_ind_val[1]}"
            cursor.execute(ex)
            data_parse_val = cursor.fetchone()
            print(data_parse_val)
            print(index)
            index += 1

            # если мы можем обработать строку из таблицы valQuery
            if data_parse_val is not None:
                # обновляем значение для следующего шага
                ex_update = f'update last_index set index1 = {cursor_ind_val[1] + 1} where id = 0'
                cursor.execute(ex_update)
                conn.commit()

                # отправляем данные на парсинг
                start_parse_module(model=data_parse_val[1], screen=data_parse_val[3], px1=data_parse_val[4],
                                   px2=data_parse_val[5], ram=data_parse_val[6], hd=data_parse_val[7], id=data_parse_val[0])

                print(data_parse_val[1])

            time.sleep(2)
# точка входа
start_infinite_loop()
