import requests
from bs4 import BeautifulSoup
import pymongo
from clear_mongo_data import start_clear_mongo

#  подключаемся к mongo
def mongo_connect(user, password):
    client = pymongo.MongoClient(
        f"mongodb+srv://{user}:{password}@cluster0.wu3cu.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
    conn = client.cold.html_page_first

    # перед каждым добавление удаляем старые записи
    conn.delete_many({})
    return conn


# получаем блоки "холодных" данных
def get_cold_parsing(link, target_name=None):
    device_class = 'product_data__gtm-js product_data__pageevents-js ' \
                   'ProductCardHorizontal js--ProductCardInListing js--ProductCardInWishlist'

    price_class = 'ProductCardHorizontal__price_current-price js--ProductCardHorizontal__price_current-price'
    response = requests.get(link)

    # сохраним данные с каждой страницы
    block_all_paginator = []
    link_this_target = []

    soup = BeautifulSoup(response.text, 'lxml')

    # первое добавление блока
    block = soup.find_all('div', class_=device_class)
    price = soup.find_all('span', class_=price_class)

    block_all_paginator.append([block, price])

    # находим все ссылки
    paginators = soup.find_all('a', class_='PaginationWidget__page js--PaginationWidget__page '
                                           'PaginationWidget__page_next PaginationWidget__page-link', href=True)
    for new_page_link in paginators:
        link_this_target.append(new_page_link['href'])

    print(link_this_target)

    # проходимся по каждой странице
    for step_to_page in link_this_target:
        response = requests.get(step_to_page)
        soup = BeautifulSoup(response.text, 'lxml')

        block = soup.find_all('div', class_=device_class)
        price = soup.find_all('span', class_=price_class)

        block_all_paginator.append([block, price])

    return block_all_paginator


# сохраняем в холодную базу html блоки
def mongo_cold_data(user, password, target_name, screen, px1, px2, ram, hd, id):
    links = f'https://www.citilink.ru/catalog/smartfony/?f=available.' \
            f'all%2Cdiscount.any%2Crating.any%2C{target_name}&pf=discount.any%2Crating.any%2C{target_name}'

    if user is None or password is None:
        raise Exception('Введите логин или пароль')

    if links is None:
        links = 'https://www.citilink.ru/catalog/smartfony/'

    if target_name is None:
        target_name = 'samsung'

    # вернули подключение к  mongo
    conn = mongo_connect(user, password)

    # вернули массив "холодных" блоков для парсинга
    cold_mass = get_cold_parsing(links)

    # записали в mongo все записи с сайта по искомому запросу
    for block_cold in cold_mass:
        # for html_page in block_cold[0]:
        #     conn.insert_one({'htmls': str(html_page)})
        for index in range(len(block_cold[0])):
            try:
                conn.insert_one({'htmls': str(block_cold[0][index]),
                                 'price': str(block_cold[1][index])})
            except:
                pass

    # начинаем очищать данные
    start_clear_mongo(screen, px1, px2, ram, hd, id)

# входная точка для парсинга
def start_parse_module(model, screen, px1, px2, ram, hd, id):
    try:
        mongo_cold_data('evgenii', 'test123', model, screen, px1, px2, ram, hd, id)
    except (Exception) as e:
        print(e)



