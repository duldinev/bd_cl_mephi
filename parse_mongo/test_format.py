import requests
from bs4 import BeautifulSoup
import pymongo
from bson import ObjectId

client = pymongo.MongoClient(
        f"mongodb+srv://evgenii:test123@cluster0.wu3cu.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
conn = client.cold.html_page

filter = {'_id' : {'$eq' : ObjectId('61a0deffa215427057617819')}}

for el in conn.find(filter):
    soup = BeautifulSoup(el['htmls'], 'lxml')
    temp = soup.find_all('span', class_='ProductCardHorizontal__properties_value')
    for els in temp:
        print(els.text.strip())