import pandas as pd
import numpy as np
import mysql.connector
from mysql.connector import connect, Error
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import Lasso

# модуль для обучения регресии
def start_predict(screen=6.5, px1=2340, px2=1080, ram=4, hd=64, id=1):
    conn = connect(
        host="84.201.148.170",
        user='node2',
        password='Node1234!',
        database='user_free')

    cursor = conn.cursor()
    ex = 'select * from parse_data'
    cursor.execute(ex)
    mass_val = cursor.fetchall()

    df = pd.DataFrame(mass_val)
    df = df.drop(df.columns[0], axis=1)
    df.columns = ['os', 'screen', 'px1', 'px2', 'ram', 'hd', 'price']
    df['os'] = pd.get_dummies(df['os'])

    features = df.drop(['price', 'os'], axis=1)
    target = df['price']

    scaler = StandardScaler()
    scaler.fit(features)
    features = scaler.transform(features)

    model = Lasso()
    model.fit(features, target)
    prediction = model.predict([[screen, px1, px2, ram, hd]])
    ex = f'update result_val set dev1val = {prediction[0]} where id = {id}'

    cursor.execute(ex)
    conn.commit()
