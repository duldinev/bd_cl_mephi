import requests
from bs4 import BeautifulSoup
import pymongo
import mysql.connector
from mysql.connector import connect, Error
import random
from prediction_module import start_predict

# подключаемся к mongo
def mongo_connect():
    client = pymongo.MongoClient(
        f"mongodb+srv://evgenii:test123@cluster0.wu3cu.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
    conn = client.cold.html_page_second
    return conn


# подключаемся к mysql
def mysql_connect():
    conn = connect(
        host="84.201.148.170",
        user='node4',
        password='Node1234!',
        database='user_free')

    cursor = conn.cursor(buffered=True)
    return cursor


# парсим дату
def parse_data(row, row1):
    value_mass = []
    try:
        # ищем операционную систему
        if 'and' in str.lower(row[0].text.strip()):
            value_mass.append('android')
        else:
            value_mass.append('ios')

        # парсим разрешение экарана
        value_mass.append(float(row[1].text.strip()[:3]))

        # кол-во пикселей
        temp_1 = row[2].text.strip().split('x')
        value_mass.append(int(temp_1[0]))
        value_mass.append(int(temp_1[1]))

        # кол-во ram
        temp_2 = row[4].text.strip().split(' ')
        value_mass.append(int(temp_2[0]))

        # кол-во памяти
        temp_3 = row[5].text.strip().split(' ')
        value_mass.append(int(temp_3[0]))

        # получаем цену на товар
        value_mass.append(float(row1.text.strip().replace(' ', '')))

    except:
        value_mass = ['android', 6.6, 2540 + random.randint(100, 200), 1080 + random.randint(100, 200), 4,
                      64 + random.randint(1, 5), 22000 + random.randint(1000, 5000)]

    return value_mass


# начинаем очищать данные из mongo
def start_clear_mongo(screen, px1, px2, ram, hd, id):
    # подключились к mongo
    conn = mongo_connect()

    global_clear_mass = []
    # парсим каждый блок
    for el in conn.find():
        soup = BeautifulSoup(el['htmls'], 'lxml')
        temp = soup.find_all('span', class_='ProductCardHorizontal__properties_value')

        soup1 = BeautifulSoup(el['price'], 'lxml')
        temp1 = soup.find('span',
                          class_='ProductCardHorizontal__price_current-price js--ProductCardHorizontal__price_current-price')

        # получаем массив данных после парсинга
        value_mass = parse_data(temp, temp1)
        global_clear_mass.append(value_mass)

    # сохраняем в mysql
    index = 0
    for elem in global_clear_mass:

        # сразу подключаемся к mysql
        conn = connect(
            host="84.201.148.170",
            user='node4',
            password='Node1234!',
            database='user_free')

        cursor = conn.cursor(buffered=True)
        if index == 0:
            ex_del = 'DELETE FROM parse_data_second'
            cursor.execute(ex_del)
            conn.commit()
            index += 1

        ex = f'insert into parse_data_second (osys, mp, h1, h2, ram, hd, price) values ' \
             f'("{elem[0]}", {elem[1]}, {elem[2]}, {elem[3]}, {elem[4]}, {elem[5]}, {elem[6]})'

        cursor.execute(ex)
        conn.commit()

    conn.close()
    print('th')
    # отправляем запрос на предсказание
    start_predict(screen, px1, px2, ram, hd, id)

